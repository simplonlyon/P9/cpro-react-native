import React from 'react';
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from './screens/HomeScreen';
import TodoScreen from './screens/TodoScreen';
import PictureScreen from './screens/PictureScreen';
import SimplonRunScreen from './screens/SimplonRunScreen';
import Simpluka from './screens/Simpluka';

const Drawer = createDrawerNavigator();

export default function App() {
  
  return (
    <NavigationContainer>
      <Drawer.Navigator>
        <Drawer.Screen component={HomeScreen} name="Home" />
        <Drawer.Screen component={TodoScreen} name="Todo" />
        <Drawer.Screen component={PictureScreen} name="Picture" />
        <Drawer.Screen component={SimplonRunScreen} name="SimplonRun" />
        <Drawer.Screen component={Simpluka} name="Simpluka" />

      </Drawer.Navigator>
    </NavigationContainer>
  );
}
