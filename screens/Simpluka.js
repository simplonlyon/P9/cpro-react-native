import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, Button, Image } from 'react-native'
import { BarCodeScanner } from 'expo-barcode-scanner';
import Nav from '../components/Nav';

const Simpluka = () => {
    const [hasPermission, setHasPermission] = useState(null);
    const [scan, setScan] = useState(null);
    const [product, setProduct] = useState({
        name: '',
        nutriscore: '',
        picture: ''
    })

    useEffect(() => {

        BarCodeScanner.requestPermissionsAsync().then(({ status }) => {
            setHasPermission(status === 'granted');
        });

    });


    const handleScan = ({data}) => {
        setScan(data);
        fetch('https://world.openfoodfacts.org/api/v0/product/'+data+'.json')
        .then(response => response.json())
        .then(data => {
            if(data.status) {
                setProduct({
                    name: data.product.product_name,
                    nutriscore: data.product.nutriscore_grade,
                    picture: data.product.image_url
                });
            }
        })
    }

    if (hasPermission === null) {
        return <View />;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }
    return (
        <View style={{ flex: 1 }}>

            <Nav title="Simpluka" />
            {scan && product.name &&
                <>
                    <Text style={{flex:1}}>{product.name}</Text>
                    <Text style={{flex:1}}>Nutriscore : {product.nutriscore}</Text>
                    <Image style={{flex:5}} source={{uri:product.picture}} />
                    <Button style={{flex:1}} title="Take again" onPress={() => setScan(null)} />
                </>
            }
            {!scan &&
                <>
                    <BarCodeScanner 
                    style={StyleSheet.absoluteFillObject}
                    onBarCodeScanned={scan ? undefined : handleScan} />
                </>
            }
        </View>
    )
}

export default Simpluka

const styles = StyleSheet.create({})
