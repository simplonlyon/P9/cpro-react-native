import React, { useState } from 'react'
import { StyleSheet, View, Button, Animated } from 'react-native'
import { Accelerometer } from "expo-sensors";
import { FlingGestureHandler, Directions, State } from 'react-native-gesture-handler';

const SimplonRunScreen = () => {
    const [squarePos, setSquarePos] = useState({ bottom: 20, left: '40%' });
    const [animation, setAnimation] = useState(new Animated.Value(0));
    const [heightAnimation, setHeightAnimation] = useState(new Animated.Value(100));
    const [isAction, setIsAction] = useState(false);

    const toggleAccel = () => {
        Accelerometer.addListener(accelData => {
            let left = ((accelData.x + 1) * 50) + '%';
            setSquarePos({ ...squarePos, left });
        });
    }

    const upFlingHandler = (event) => {
        if (!isAction && event.nativeEvent.state === State.ACTIVE) {
            setIsAction(true);
            Animated.sequence([Animated.timing(animation, {
                toValue: -500,
                duration: 1000
            }),
            Animated.spring(animation, {
                toValue: 0,
                friction: 4,
                tension: 20,
                duration: 2000
            })]).start(() => setIsAction(false));
        }
    }
    const downFlingHandler = (event) => {
        if (!isAction && event.nativeEvent.state === State.ACTIVE) {
            setIsAction(true);
            Animated.sequence([Animated.timing(heightAnimation, {
                toValue: 20,
                duration: 1000
            }),
            Animated.timing(heightAnimation, {
                toValue: 100,
                duration: 1000
            })]).start(() => setIsAction(false));
        }
    }


    return (
        <FlingGestureHandler

            direction={Directions.DOWN}
            onHandlerStateChange={downFlingHandler}
        >
            <FlingGestureHandler
                direction={Directions.UP}
                onHandlerStateChange={upFlingHandler}
            >
                <View style={{ flex: 1 }}>
                    <Animated.View style={{
                        ...styles.square, ...squarePos,
                        transform: [{ translateY: animation }],
                        height: heightAnimation
                    }} />

                    <Button title="Go !" onPress={toggleAccel}></Button>
                </View>
            </FlingGestureHandler>
        </FlingGestureHandler>
    )
}

export default SimplonRunScreen

const styles = StyleSheet.create({
    square: {
        //To make Square Shape
        width: 100,
        height: 100,
        position: 'absolute',
        backgroundColor: 'red',
    },
})
