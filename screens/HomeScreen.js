

import React from 'react'
import { View, Text, Button } from 'react-native';
import { Header } from "react-native-elements";
import Nav from '../components/Nav';

const HomeScreen = ({navigation}) => {
    return (
        <View>
            <Nav title="Home" />
            
            <Text>Welcome to screen</Text>
            <Button title="Go To Todo" onPress={() => navigation.navigate('Todo')} />
        </View>
    )
}

export default HomeScreen
