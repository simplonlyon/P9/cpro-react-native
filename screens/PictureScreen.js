
import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { Camera } from 'expo-camera';
import { Button } from 'react-native-elements';

const PictureScreen = () => {
    const [hasPermission, setHasPermission] = useState(null);
    const [picture, setPicture] = useState(null);
    let camera;

    useEffect(() => {

        Camera.requestPermissionsAsync().then(({ status }) => {
            setHasPermission(status === 'granted');
        });

    });


    const handlePicture = () => {
        if (camera) {
            camera.takePictureAsync({ base64: true }).then(data => {
                setPicture(data.base64);
            })
        }
    }

    if (hasPermission === null) {
        return <View />;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }
    return (
        <View style={{ flex: 1 }}>
            {picture &&
                <>
                    <Image style={{flex:1}} source={{uri:`data:image/jpeg;base64,${picture}`}} />
                    <Button title="Take again" onPress={() => setPicture(null)} />
                </>
            }
            {!picture &&
                <>
                    <Camera ref={ref => camera = ref} style={{ flex: 1 }} type={Camera.Constants.Type.back} />
                    <Button title="Take Picture" onPress={handlePicture} />
                </>
            }
        </View>
    )
}

export default PictureScreen

const styles = StyleSheet.create({})
