import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import TodoList from '../components/TodoList'

import Nav from '../components/Nav'

const TodoScreen = () => {
    return (
        <View style={styles.container}>

            <Nav title="Todo List" />
            <TodoList />
        </View>
    )
}

export default TodoScreen

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
