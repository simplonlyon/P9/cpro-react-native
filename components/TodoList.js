import React, { useState } from 'react'
import { View, Text, TextInput, StyleSheet, StatusBar, CheckBox } from 'react-native'
import { Button } from 'react-native-elements';



let id = 4;

export default function TodoList() {
    const [list, setList] = useState([
        {id: 1, task: 'shopping', done: false},
        {id: 2, task: 'couch', done: true},
        {id: 3, task: 'eat', done: false},
    ]);
    const [task, setTask] = useState('');

    const addTodo = () => {
        setList([...list, 
            {id: id++, task, done: false}
        ]);
    };

    const handleChange = text => {
        setTask(text);
    };

    const checkItem = item => {
        setList(list.map(todo => {
            if(todo.id == item.id) {
                return {
                    ...todo,
                    done: !todo.done
                }
            }
            return todo
        }))
    };

    return (
        <View style={styles.container}>

            <View style={styles.form}>
                <TextInput style={styles.controls} 
                placeholder="New task"
                value={task}
                onChangeText={handleChange}></TextInput>
                <View style={styles.controls}>
                    <Button  
                    onPress={addTodo}
                    title="Add"></Button>
                </View>
            </View>
            <View style={styles.list}>
                {list.map(item => (
                <View key={item.id} style={styles.item}>
                    <Text style={item.done && styles.itemCheck}>{item.task}</Text>
                    <CheckBox onChange={() => checkItem(item)} value={item.done} />
                </View>
                )
                )}        
            </View>
            <Button style={styles.addIcon} icon={{name: 'add'}} rounded={true} />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    }, 
    form: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center'
    },
    item: {
      flexDirection: 'row',
      justifyContent: 'center'
    },
    itemCheck: {
        textDecorationLine: 'line-through'
    }, 
    controls: {
        width: '50%',
        height: 30,
        marginLeft: 10
    },
    list: {
        flex: 4,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    addIcon: {
        position: 'absolute',
        top: 10,
        left: 10,
        width: 30,
        height: 30
    }
  });
  